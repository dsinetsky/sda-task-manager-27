package ru.t1.dsinetsky.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.comparator.CreatedComparator;
import ru.t1.dsinetsky.tm.comparator.NameComparator;
import ru.t1.dsinetsky.tm.comparator.StatusComparator;
import ru.t1.dsinetsky.tm.exception.system.InvalidSortTypeException;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) throws InvalidSortTypeException {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (value.equals(sort.name())) return sort;
        }
        throw new InvalidSortTypeException();
    }

    @NotNull
    public static String getSortList() {
        @NotNull final StringBuilder sortList = new StringBuilder();
        for (@NotNull final Sort sort : values()) {
            sortList.append(" - ").append(sort.name()).append("\n");
        }
        return sortList.toString();
    }

}
