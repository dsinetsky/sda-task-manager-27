package ru.t1.dsinetsky.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static String getStatusList() {
        @NotNull final StringBuilder statusName = new StringBuilder();
        for (@NotNull final Status status : values())
            statusName.append(" - ").append(status.name()).append("\n");
        return statusName.toString();
    }

    @NotNull
    public static Status toStatus(@Nullable final String value) throws InvalidStatusException {
        if (value == null || value.isEmpty()) throw new InvalidStatusException();
        for (@NotNull final Status status : values())
            if (value.equals(status.name())) return status;
        throw new InvalidStatusException();
    }

}
