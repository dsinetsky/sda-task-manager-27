package ru.t1.dsinetsky.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.model.IWBS;
import ru.t1.dsinetsky.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @Nullable
    private String desc = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(@NotNull final String name, @Nullable final String desc) {
        this.name = name;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(name).append(" : ").append(desc).toString();
    }

}
