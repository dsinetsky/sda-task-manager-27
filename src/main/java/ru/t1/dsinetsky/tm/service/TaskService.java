package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;
import java.util.Random;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    @NotNull
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (!existsById(userId, id)) throw new TaskNotFoundException();
        @NotNull final Task task = findById(userId, id);
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    @NotNull
    public Task updateByIndex(@Nullable final String userId, final int index, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    @NotNull
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        if (!existsById(userId, id)) throw new TaskNotFoundException();
        if (status == null) throw new InvalidStatusException();
        @NotNull final Task task = findById(userId, id);
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public Task changeStatusByIndex(@Nullable final String userId, final int index, @Nullable final Status status) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        if (status == null) throw new InvalidStatusException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public List<Task> returnTasksOfProject(@Nullable final String userId, @Nullable final String projectId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        return repository.findTasksByProjectId(userId, projectId);
    }

    @Override
    public void createTest(@NotNull final User user) throws GeneralException {
        @NotNull final Random randomizer = new Random();
        final int count = 10;
        @NotNull final String name = user.getLogin() + "-task";
        @NotNull final String description = "Task for " + user.getLogin() + " number ";
        for (int i = 1; i < count; i++) {
            @NotNull final Task task = create(user.getId(), name + randomizer.nextInt(11), description + i);
            @Nullable final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            task.setStatus(status);
        }
    }

}
