package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M> {

    @NotNull
    List<M> returnAll();

    @NotNull
    List<M> returnAll(Comparator comparator);

    @Nullable
    M add(@NotNull M model) throws GeneralException;

    @Nullable
    Collection<M> add(@NotNull Collection<M> models) throws GeneralException;

    @Nullable
    Collection<M> set(@NotNull Collection<M> models) throws GeneralException;

    void clear() throws GeneralException;

    @Nullable
    M findById(@NotNull String id) throws GeneralException;

    @Nullable
    M findByIndex(int index) throws GeneralException;

    @Nullable
    M removeById(@NotNull String id) throws GeneralException;

    @Nullable
    M removeByIndex(int index) throws GeneralException;

    boolean existsById(@NotNull String id) throws GeneralException;

    int getSize();

}
