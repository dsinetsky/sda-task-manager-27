package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password) throws GeneralException;

    void login(@Nullable String login, @Nullable String password) throws GeneralUserException;

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId() throws GeneralUserException;

    @NotNull
    User getUser() throws GeneralException;

    void checkRoles(@Nullable Role[] roles) throws GeneralException;

    void lockUserByLogin(@Nullable String login) throws GeneralException;

    void unlockUserByLogin(@Nullable String login) throws GeneralException;

}
