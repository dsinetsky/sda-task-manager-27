package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String desc);

}
