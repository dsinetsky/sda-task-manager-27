package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findUserByLogin(@NotNull String login);

    boolean isUserLoginExist(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
