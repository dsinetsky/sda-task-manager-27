package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IAuthService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
