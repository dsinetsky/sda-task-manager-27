package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_COMPLETE_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Completes task (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
        showTask(task);
        System.out.println("Task successfully completed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
