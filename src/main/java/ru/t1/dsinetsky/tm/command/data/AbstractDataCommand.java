package ru.t1.dsinetsky.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.dto.Domain;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(getServiceLocator().getProjectService().returnAll());
        domain.setTasks(getServiceLocator().getTaskService().returnAll());
        domain.setUsers(getServiceLocator().getUserService().returnAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws GeneralException {
        if (domain == null) return;
        getServiceLocator().getProjectService().set(domain.getProjects());
        getServiceLocator().getTaskService().set(domain.getTasks());
        getServiceLocator().getUserService().set(domain.getUsers());
        getServiceLocator().getAuthService().logout();
        System.out.println("You are logged off!");
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
