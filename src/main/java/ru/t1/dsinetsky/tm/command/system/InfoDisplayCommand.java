package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.util.NumberUtil;

public final class InfoDisplayCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_INFO;

    @NotNull
    public static final String NAME = TerminalConst.CMD_INFO;

    @NotNull
    public static final String DESCRIPTION = "Shows system info";

    @Override
    public void execute() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        /* Memory as bytes */
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        /* Memory as String */
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat);
        @NotNull final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        @NotNull final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        @NotNull final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        /* Print memory */
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used Memory: " + usedMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
