package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_SHOW_USER;

    @NotNull
    public static final String DESCRIPTION = "Shows information about current user";

    @Override
    public void execute() throws GeneralException {
        @NotNull final User user = getAuthService().getUser();
        System.out.println(user);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
