package ru.t1.dsinetsky.tm.util;

public interface NumberUtil {

    static String formatBytes(final long bytes) {
        final long kilobyte = 1024;
        final long megabyte = 1024 * kilobyte;
        final long gigabyte = 1024 * megabyte;
        final long terabyte = 1024 * gigabyte;
        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return bytes / kilobyte + " Kb";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return bytes / megabyte + " Mb";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return bytes / gigabyte + " Gb";
        } else {
            return bytes / terabyte + " Tb";
        }
    }

}
