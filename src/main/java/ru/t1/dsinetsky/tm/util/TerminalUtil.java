package ru.t1.dsinetsky.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.system.InvalidValueException;
import ru.t1.dsinetsky.tm.service.LoggerService;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        @Nullable final String line = SCANNER.nextLine();
        LoggerService.getMessageLogger().fine(line);
        return line;
    }

    static int nextInt() throws InvalidValueException {
        try {
            @Nullable final String value = nextLine();
            LoggerService.getMessageLogger().fine(value);
            if (value == null) throw new InvalidValueException();
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new InvalidValueException();
        }
    }

}
